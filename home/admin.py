from django.contrib import admin
from home.models import Home, Category
# Register your models here.
 
class HomeAdmin(admin.ModelAdmin):
        exclude = ['posted']
        prepopulated_fields = { 'slug' : ('title',)}
 
class CategoryAdmin(admin.ModelAdmin):
        prepopulated_fields = { 'slug' : ('title',)}

admin.site.register(Home, HomeAdmin)
admin.site.register(Category, CategoryAdmin)

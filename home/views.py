from django.shortcuts import render
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from home.models import Home, Category
# Create your views here.
 
def index(request):
        return render_to_response('home/index.html', context=RequestContext(request))
 
def blog(request):
        return render_to_response('home/blog.html', {
                'categories' : Category.objects.all(),
                'posts' : Home.objects.all()[:5]
                })
 
def view_post(request, slug):
        return render_to_response('home/post.html', {
                'post' : get_object_or_404(Home, slug=slug)
                })
 
def view_category(request, slug):
	category = get_object_or_404(Category, slug=slug)
        return render_to_response('home/blog.html', {
                'category' : category,
                'posts' : Home.objects.filter(category=category)[:5],
		'categories' : Category.objects.all(),
      })
